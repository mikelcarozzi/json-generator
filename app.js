const xlsx = require('node-xlsx').default;
const _ = require('lodash');

const sheet = xlsx.parse(`${__dirname}/Traducciones-Web.xlsx`);

const videoChat = sheet[1].data
const auth = sheet[2].data
const dashboard = sheet[3].data
const salaEspera = sheet[4].data
const agenda = sheet[4].data
const soporte = sheet[6].data
const common = sheet[7].data
const fichaClinica = sheet[8].data
const perfil = sheet[9].data
const cuenta = sheet[10].data

let dashKeys = []
let _key, _value
let _els = {}
let _do


function parse(obj) {
    obj.shift()

    return new Promise((resolve, reject) => {
        for (var i = 0; i < obj.length; i++) {
            let pair = obj[i]

            if (pair.length > 0) {
                for (let j = 0; j < pair.length; j++) {
                    if (j % 2 == 0) {
                        _key = pair[j]
                    } else {
                        _value = pair[j]
                    }
                }

                dashKeys.push({ key: _key, value: _value })
            }
        }

        for (let i = 0; i < dashKeys.length; i++) {
            const el = dashKeys[i];

            if (el.key) {
                let _dots = el.key.replace(/-/g, '.')
                let str = _dots.split('.')

                _do = f(_dots, el.value)
                _els = _.merge(_els, _do)
            }
        }

        resolve()
    })
}

function f(key, value) {
    var result = object = {};
    var arr = key.split('.');

    for (var i = 0; i < arr.length - 1; i++) {
        object = object[arr[i]] = {};
    }

    object[arr[arr.length - 1]] = value;

    return result;
}

let promises = [
    parse(videoChat),
    parse(auth),
    parse(dashboard),
    parse(salaEspera),
    parse(agenda),
    parse(soporte),
    parse(common),
    parse(fichaClinica),
    parse(perfil),
    parse(cuenta)
]


Promise.all(promises)
    .then(() => {
        console.log(JSON.stringify(_els, null, 2));
    })